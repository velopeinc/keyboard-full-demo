import { Lightning, Utils } from '@lightningjs/sdk';
import Keyboard from "./keyboard/Keyboard.js";
import TextInput from "./form/TextInput.js";

export default class App extends Lightning.Component {
    static getFonts() {
        return [];
    }
    static _template() {
        return {
            Blur:{
                rect: true, type: Lightning.components.FastBlurComponent, w:1920, h:1080, amount: 0, content: {
                    Image: {
                        src: Utils.asset("cave.jpg")
                    },
                    Form:{ x:100, y: 200,
                        Search:{ alpha:1,
                            type: TextInput, placeholder:'Press ok to start..', label:"Search"
                        },
                        Password:{ y: 200, alpha:0.3, scale:0.8,
                            type: TextInput, placeholder:'Press ok for keyboard', label:"Password"
                        },
                        Shout:{ y: 400, alpha:0.3, scale:0.8,
                            type: TextInput, placeholder:'Press ok to shout!', label:"SHOUT!"
                        }
                    }
                }
            },
            Wrapper:{
                rect:true, color: 0xaa000000, w:1200, h:800, x: 960, y: 1080, mount:0.5, alpha:0,
                Keyboard:{
                    type: Keyboard,
                    mode: "normal",
                    title:"search".toUpperCase(),
                    message:"At least 5 characters",
                    emailHelpers: true,
                    email:["@gmail.com","@outlook.com","@hotmail.com","@comcast.net"]
                    // mask:Keyboard.mask.PASSWORD
                }
            }

        }
    }

    _init(){
        this._content = this.tag("Blur").content;

        // store references to each inputfield
        this._searchInput = this._content.tag("Search");
        this._passwordInput = this._content.tag("Password");
        this._shoutInput = this._content.tag("Shout");

        // will fire on every character update
        this.tag("Keyboard").on("change",({char, value})=>{

        });

        // will fire when done key is pressed in keyboard
        this.tag("Keyboard").on("done",({value})=>{
            this.onDone(value);
        });

        // force focus to SearchInput
        this._setState("SearchInput");
    }

    /**
     * States are to explain how the keyboard can behave under different
     * circumstances. In a real-world example all inputs don't need
     * their own state, you just traverse through a set of children.
     * @returns {*[]}
     * @private
     */
    static _states(){
        return [
            class SearchInput extends this{
                $enter(){
                    // store reference so we can apply the value
                    this._input = this._searchInput;

                    this._searchInput.patch({
                        smooth:{alpha:1, scale:1}
                    });
                }
                _handleEnter(){
                    this._setState("Keyboard")
                }
                $exit(){
                    this._searchInput.patch({
                        smooth:{alpha:0.3, scale:0.8}
                    });
                }
                _handleDown(){
                    this._setState("PasswordInput");
                }
            },
            class PasswordInput extends this{
                $enter(){
                    // store reference so we can apply the value
                    this._input = this._passwordInput;

                    this._passwordInput.patch({
                        smooth:{alpha:1, scale:1}
                    });
                }
                $exit(){
                    this._passwordInput.patch({
                        smooth:{alpha:0.3, scale:0.8}
                    });
                }
                _handleUp(){
                    this._setState("SearchInput");
                }
                _handleDown(){
                    this._setState("ShoutInput");
                }
                _handleEnter(){
                    this._setState("Keyboard")
                }
            },
            class ShoutInput extends this{
                $enter(){
                    // store reference so we can apply the value
                    this._input = this._shoutInput;

                    this._shoutInput.patch({
                        smooth:{alpha:1, scale:1}
                    });
                }
                $exit(){
                    this._shoutInput.patch({
                        smooth:{alpha:0.3, scale:0.8}
                    });
                }
                _handleUp(){
                    this._setState("PasswordInput");
                }
                _handleEnter(){
                    this._setState("Keyboard")
                }
            },
            class Keyboard extends this{
                $enter({prevState}){
                    // store state so we got a returning point
                    this._keyboardReturnState = prevState;

                    // force keyboard value to be the one stored
                    this.tag("Keyboard").value = this._input.value;

                    this.patch({
                        Blur:{
                            smooth:{
                                amount:[4,{duration:1}]
                            }
                        },
                        Wrapper:{
                            smooth:{alpha:1, y:540}
                        }
                    });
                }
                $exit(){
                    this.patch({
                        Blur:{smooth:{amount:0}},
                        Wrapper:{smooth:{alpha:0, y:1080}}
                    });
                }
                onDone(value){
                    this._input.value = value;
                    this._setState(this._keyboardReturnState);
                }
                _getFocused(){
                    return this.tag("Keyboard");
                }

                _handleBack(){
                    this._setState(this._keyboardReturnState)
                }
            }
        ]
    }
}


