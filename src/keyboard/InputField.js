import { Lightning } from '@lightningjs/sdk';

export default class InputField extends Lightning.Component {

    static _template() {
        return {
            Value: {
                text: {text: '', textColor: 0xEEffffff}
            },
            Cursor: {
                rect: true, color:0xffffffff, h: 40, w: 4, y: 7
            },
            Border: {
                rect: true, w: w => w, h: 2, color: 0x50ffffff, y: 70
            },
            /**
             * Hidden value is used for measuring where the cursor should
             * be positioned when a user changes the cursor position. Since
             * the text is one texture we don't know the exact position of each
             * individual character, so we render a substring version of the text
             * to determine the renderwidth and position the cursor based on that number
             */
            HiddenValue: {
                alpha:0.00001,
                text: {text: '', textColor: 0xEEffffff}
            }
        };
    }

    _construct(){
        /**
         * input value
         * @type {string}
         * @private
         */
        this._value = '';

        /**
         * Cursor position in string
         * @type {number}
         * @private
         */
        this._cursorPosition = 0;
    }

    _init() {
        this._blink = this.tag("Cursor").animation({
            duration: 1, repeat: -1, actions: [
                {p: 'alpha', v: {0: 1, 0.5: 0, 1: 1}}
            ]
        });
    }

    _focus(){
        this.patch({
            Cursor:{smooth:{w:3}},
            Border:{smooth:{color:0xffffffff, h:6}}
        });
    }

    _unfocus(){
        this.patch({
            Cursor:{smooth:{w:4}},
            Border:{smooth:{color:0x50ffffff, h:2}}
        });
    }


    _handleLeft(){
        const pos = this._cursorPosition;
        if(pos > 0){
            const position = this._calculatePositionByIndex(this._value, pos-1);
            this.tag("Cursor").setSmooth("x",position);
            --this._cursorPosition;
        }
    }

    _handleRight(){
        const pos = this._cursorPosition;
        if(pos < this._value.length){
            const position = this._calculatePositionByIndex(this._value, pos+1);
            this.tag("Cursor").setSmooth("x",position);
            ++this._cursorPosition;
        }
    }

    _handleDirection(direction){

    }

    _active() {
        this._blink.start();
    }

    _inactive() {
        this._blink.stop();
    }

    feed(str) {
        let value = this._value;
        let position = this._cursorPosition;

        if(position < value.length){
            const start = value.substring(0,position);
            const end = value.substring(position, value.length);
            value = `${start}${str}${end}`;
        }else{
            value += `${str}`;
        }

        return this.change(value, str);
    }

    /**
     * update the actual text
     * @param value - new input value
     * @param addition - latest addition to string
     * @returns {*}
     */
    change(value, addition){
        const position = this._cursorPosition;
        const currentValue = this._value;

        this.tag("Value").text.text = this._applyMask(value);
        this.tag("Value").loadTexture();

        if(value.length === 0){
            this._cursorPosition = 0;
            this.tag("Cursor").setSmooth("x", 0);
        }else{
            // if we invoke text on custom position
            if(position < currentValue.length){
                const customIndex = addition === -1?position-1:position+addition.length;
                const customPosition = this._calculatePositionByIndex(value, customIndex);

                this.tag("Cursor").setSmooth("x", customPosition);
                this._cursorPosition = customIndex;
            }else{
                this.tag("Cursor").setSmooth("x", this.tag("Value").renderWidth + 4);
                this._cursorPosition = value.length;
            }
        }

        return this._value = value;
    }

    delete(){
        let value = this._value;
        let position = this._cursorPosition;

        if(position < value.length){
            const start = value.substring(0,position-1);
            const end = value.substring(position, value.length);
            value = `${start}${end}`;

            return this.change(value, -1);
        }else{
            return this.change(value.substring(0,value.length-1));
        }
    }

    _calculatePositionByIndex(str, index){
        const value = str.substring(0,index);

        // update hidden value and calc width
        this.tag("HiddenValue").text.text = value;
        this.tag("HiddenValue").loadTexture();

        return this.tag("HiddenValue").renderWidth;
    }

    get value() {
        return this._value;
    }

    _applyMask(str){
        if(typeof this._mask === "function"){
            return this._mask.call(null,str);
        }else{
            return str;
        }
    }

    set mask(v){
        this._mask = v;
    }

    set resetValue(v){
        // clear
        this.change("");
        // force new value
        this.change(v);
    }
}