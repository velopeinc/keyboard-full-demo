import { Lightning } from '@lightningjs/sdk';
import Actions from "./Actions.js";

export default class HotKeys extends Actions{

    static _template(){
        return {
            w:400,
            flex:{
                direction:'column', justifyContent:'space-around'
            }
        }
    }

    _init(){
        this.cparent.on("layoutChange",this.onLayoutChange.bind(this))
    }

    create(config, constructor){
        const keys = Object.keys(config);
        this.children = keys.map((label)=>{
            const action = config[label];
            if(typeof action === "string"){
                return {
                    type: constructor, label, action:config[label]
                }
            }else{
                return {
                    type: constructor, identifier: label, actions:config[label]
                }
            }
        });
    }

    onLayoutChange({layout}){
        this.children.filter(child=>child.actions).forEach((child)=>{
            const {active, inactive} = child.actions || {};
            if(child.identifier === layout){
                child.label = active.label;
                child.action = active.action;
            }else if(child.actions.hasOwnProperty(layout)){
                child.label = child.actions[layout].label;
                child.action = child.actions[layout].action;
            }else{
                child.label = inactive.label;
                child.action = inactive.action;
            }
        });
    }

    _handleEnter(){
        return false;
    }

    _handleUp(){
        return super._handleLeft();
    }

    _handleDown(){
        return super._handleRight();
    }

    _handleLeft(){
        // let bubble
        return false;
    }

    _handleRight(){
        // let bubble
       return false;
    }
}