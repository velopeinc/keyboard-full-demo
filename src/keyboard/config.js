export const config = {
    layout: {
        normal: [
            '1', '2', '3', '4', '5', '6', '7', '8', '9', '0',
            'q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p',
            'a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '@',
            'z', 'x', 'c', 'v', 'b', 'n', 'm', ',', '.', '-'
        ],
        special: [
            '!', '@', '#', '$', '%', '^', '&', '*', '(', ')',
            '{', '}', '[', ']', ';', '"', '\'', '|', '\\', '/',
            '<', '>', '?', '±', '`', '~'
        ],
        accents: [
            'ä', 'ë', 'ï', 'ö', 'ü', 'ÿ', 'à', 'è', 'ì', 'ò', 'ù', 'á',
            'é', 'í', 'ó', 'ú', 'ý', 'â', 'ê', 'î', 'ô', 'û', 'ã', 'ñ'

        ],
        accentsUpper: [
            'Ä', 'Ë', 'Ï', 'Ö', 'Ü', 'Ÿ', 'À', 'È', 'Ì', 'Ò', 'Ù', 'Á',
            'É', 'Í', 'Ó', 'Ú', 'Ý', 'Â', 'Ê', 'Î', 'Ô', 'Û', 'Ã', 'Ñ'
        ],
        uppercase: [
            '!', '@', '#', '$', '%', '^', '&', '*', '(', ')',
            'Q', 'W', 'E', 'R', 'T', 'Y', 'U', 'I', 'O', 'P',
            'A', 'S', 'D', 'F', 'G', 'H', 'J', 'K', 'L', '@',
            'Z', 'X', 'C', 'V', 'B', 'N', 'M', ',', '.', '-'
        ]
    },
    actions: {
        Clear: 'onClear',
            Space: 'onSpace',
            Done: 'onDone'
    },
    hotkeys: {
        Delete: 'onDelete',
            special: {
            active: {label: 'abc', action: 'layout:normal'},
            inactive: {label: '!?#&', action: 'layout:special'}
        },
        accents: {
            active: {label: 'abc', action: 'layout:normal'},
            inactive: {label: 'àèü', action: 'layout:accents'},
            // this will trigger a button change when layout gets
            // changed to uppercase
            uppercase: {label: 'ÀÈÜ', action: 'layout:accentsUpper'}
        },
        uppercase: {
            active: {label: 'abc', action: 'layout:normal'},
            inactive: {label: 'Shift', action: 'layout:uppercase'}
        }
    }
};