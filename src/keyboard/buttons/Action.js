import { Lightning } from '@lightningjs/sdk';
export default class Action extends Lightning.Component{

    static _template(){
        return {
            flex:{direction:'row'},
            Wrapper:{
                w: Action.width, h: Action.height,
                rect:true, color:0x00ffffff,
                Label:{ mount:0.5, y:y=>y/2, x:x=>x/2,
                    text:{text:'Action', fontSize:35}
                }
            }
        }
    }

    _focus(){
        this.patch({
            Wrapper:{
                smooth:{color:0xffffffff},
                Label:{
                    smooth:{color:0xff000000, scale:1.2}
                }
            }
        });
    }

    _unfocus(){
        this.patch({
            Wrapper:{
                smooth:{color:0x00ffffff},
                Label:{
                    smooth:{color:0xffffffff, scale:1}
                }
            }
        });
    }

    set label(v){
        this._label = v;
        this.tag("Label").text.text = `${v}`;

        // todo: better implementation
        if(v === "Space"){
            this.tag("Wrapper").w = 408;
        }
    }

    set key(v){
        this._key = v;
    }

    set action(v){
        this._action = v;
    }

    get action(){
        return this._action;
    }

    static get width(){
        return 192;
    }

    static get height(){
        return 80;
    }
}