import Action from "./Action.js";

export default class Email extends Action{

    static _template(){
        const template = super._template();
        template.Wrapper.w = Email.width;
        template.Wrapper.h = Email.height;
        template.Wrapper.Label.text.fontSize = 30;
        template.Wrapper.Label.alpha = 0.8;
        return template;
    }

    _handleEnter(){
        return false;
    }

    static get width(){
        return 270;
    }

    static get height(){
        return 80;
    }
}