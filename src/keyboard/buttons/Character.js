import { Lightning } from '@lightningjs/sdk';

export default class Character extends Lightning.Component {

    static _template(){
        return {
            Wrapper:{
                w: Character.width, h: Character.height,
                Label:{ mount:0.5, y:y=>y/2, x:x=>x/2,
                    text:{text:'a', fontSize:35}
                }
            }
        }
    }

    set label(v){
        this._label = v;
        this.tag("Label").text.text = `${v}`;
    }

    get label(){
        return this._label;
    }

    static get width(){
        return 90;
    }

    static get height(){
        return 90;
    }

    _focus(){
        this.patch({
            Wrapper:{
                Label:{
                    smooth:{scale:1.5, color:0xff000000}
                }
            }
        });
    }

    _unfocus(){
        this.patch({
            Wrapper:{
                Label:{
                    smooth:{scale:1, color:0xffffffff}
                }
            }
        });
    }
}