import Action from "./Action.js";

export default class HotKey extends Action{

    static _template(){
        const template = super._template();
        template.Wrapper.w = HotKey.width;
        template.Wrapper.h = HotKey.height;
        return template;
    }

    /**
     * We provide override the _unfocus behaviour
     * because we want to make use of active hotkeys
     * @private
     */
    _unfocus(){
        this.patch({
            Wrapper:{
                smooth:{color:0x00ffffff},
                Label:{
                    smooth:{color:0xffffffff, scale:1}
                }
            }
        });
    }

    set identifier(v){
        this._identifier = v;
    }

    get identifier(){
        return this._identifier;
    }

    set actions(v){
        this._actions = v;
        if(v.hasOwnProperty("inactive")){
            this.label = v.inactive.label;
            this.action = v.inactive.action;
        }
    }

    get actions(){
        return this._actions;
    }

    static get width(){
        return 200;
    }

    static get height(){
        return 90;
    }
}