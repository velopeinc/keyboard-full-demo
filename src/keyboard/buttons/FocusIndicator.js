import { Lightning, Utils } from '@lightningjs/sdk';

export default class FocusIndicator extends Lightning.Component{

    static _template(){
        return {
            w: 96, h:99,
            Base:{
                src:Utils.asset("key-focus.png")
            },
            Surrounding:{
                Left:{
                    x:-50, y:30, alpha:0,
                    src:Utils.asset("surrouding-focus.png"),
                    rotation: Math.PI*1.5
                },
                Up:{
                    y:-40, x:23, alpha:0,
                    src:Utils.asset("surrouding-focus.png"),
                },
                Right:{
                    y:30, x:95, alpha:0,
                    src:Utils.asset("surrouding-focus.png"),
                    rotation: Math.PI/2
                },
                Down:{
                    y:110, x:23, alpha:0,
                    src:Utils.asset("surrouding-focus.png"),
                    rotation: Math.PI
                }
            }
        }
    }

    _init(){
        this._pulse = this.animation({
            duration:1.5, stopMethod:'immediate', delay:0.4, repeat:-1, actions:[
                {p:'alpha', v:{0:1, 0.5: 0.8, 1:1}},
                {p:'scale', v:{0:1, 0.5: 0.85, 1:1}}
            ]
        });
    }

    start(){
        this._pulse.start();
    }

    stop(){
        this._pulse.stop();

        this.patch({
            smooth:{alpha:0,scale:0.2}
        });
    }

    set position({row, col}){
        let surround = 0;
        const S = FocusIndicator.SURROUND;

        if(col > 0){ surround+=S.Up }
        if(col < 3){ surround+=S.Down }
        if(row > 0){ surround+=S.Left }
        if(row < 9){ surround+=S.Right }

        this.tag("Surrounding").children.forEach((el)=>{
            el.patch({
                alpha:0,smooth:{
                    alpha: (surround&S[el.ref])|0
                }
            })
        });
    }
}

FocusIndicator.SURROUND = {
    Left: 1,
    Up: 2,
    Right: 4,
    Down: 8
};