import { Lightning } from '@lightningjs/sdk';
import Indicator from "./buttons/FocusIndicator.js";

export default class InputCharacters extends Lightning.Component {

    static _template(){
        return {
            FocusIndicator:{
               type: Indicator, x:-5, y:-8
            },
            Chars:{

            }
        }
    }

    _focus(){
        this.tag("FocusIndicator").start();
    }

    _unfocus(){
        this.tag("FocusIndicator").stop();
    }

    create(config, constructor){
        const characters = config;

        // cleanup previous layout
        this.tag("Chars").childList.clear();

        // create keys
        this.tag("Chars").children = characters.map((label, idx) => {
            return {
                type: constructor,
                label,
                x: (idx % 10) * constructor.width,
                y: ~~(idx / 10) * constructor.height
            };
        });

        // reset focus index
        this._charIndex = 0;

        // store reference for later
        // focus position
        this._constructor = constructor;

        // signal ready for navigation
        this.signal("ready");

        // set initial index
        this.setIndex(0);

    }

    _handleRight(){
        const realIndex = this._charIndex;
        const rowIndex = realIndex % 10;

        if(rowIndex < 10 - 1 && realIndex < this.chars.length - 1){
            this.setIndex(realIndex+1);
        }else{
            // bubble up
            return false;
        }
    }

    _handleLeft(){
        const realIndex = this._charIndex;
        const rowIndex = realIndex % 10;

        if(rowIndex > 0){
            this.setIndex(realIndex-1);
        }else{
            // bubble up
            return false;
        }
    }

    _handleUp(){
        const realIndex = this._charIndex;
        const newIndex = realIndex - 10;

        if(newIndex >= 0){
            this.setIndex(newIndex);
        }else{
            // bubble up
            return false;
        }
    }

    _handleDown(){
        const realIndex = this._charIndex;
        const newIndex = realIndex + 10;

        if(newIndex < this.chars.length){
            this.setIndex(newIndex);
        }else{
            // bubble up
            return false;
        }
    }

    setIndex(index, fastForward){
        // capture index beyond bounds
        if(index > this.chars.length - 1){
            index = this.chars.length - 1;
        }

        const row = index%10;
        const col = ~~(index/10);

        this.tag("FocusIndicator").position = {row,col};

        // animate focus indicator
        this.tag("FocusIndicator").patch({
            smooth:{
                x: (index % 10) * this._constructor.width - 5,
                y: ~~(index / 10) * this._constructor.height - 8
            }
        });

        if(fastForward){
            this.tag("FocusIndicator").fastForward("x");
            this.tag("FocusIndicator").fastForward("y");
        }

        // store new index
        this._charIndex = index;
    }

    _getFocused(){
        return this.activeChar;
    }

    get chars(){
        return this.tag("Chars").children;
    }

    get activeChar(){
        return this.chars[this._charIndex];
    }

    get index(){
        return this._charIndex;
    }

    _handleEnter(){
        this.signal("onKeypress",{char:this.activeChar.label});
    }
}