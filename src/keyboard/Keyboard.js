import { Lightning } from '@lightningjs/sdk';
import InputField from "./InputField.js";
import InputCharacters from "./InputCharacters.js";
import EmailHelpers from "./EmailHelpers.js";
import Actions from "./Actions.js";
import HotKeys from "./HotKeys.js";

import Character from "./buttons/Character.js";
import Action from "./buttons/Action.js";
import HotKey from "./buttons/HotKey.js";
import Email from "./buttons/Email.js";
import {config} from "./config.js";

export default class Keyboard extends Lightning.Component {

    static _template() {
        return {
            w: w => w,
            Title: {
                x: 50, y: 20,
                text: {
                    text: '', fontSize: 35, textColor: 0x90ffffff
                }
            },
            InputField: {
                type: Keyboard.InputField, x: 50, y: 90, w: w => w - 100,
            },
            Message: {
                x: 50, y: 180,
                text: {
                    text: '', fontSize: 24, textColor: 0x90ffffff
                }
            },
            InputCharacters: {
                type: InputCharacters, x: 50, y: 280, signals: {
                    ready: true, onKeypress: true
                }
            },
            EmailHelpers: {
                type: EmailHelpers, y: 610, x: 50, visible: false
            },
            Actions: {
                type: Actions, y: 680
            },
            HotKeys: {
                type: HotKeys, x: 960, y: 280
            }
        };
    }

    static get config(){
        return config;
    }

    /**
     * Returns the Component for inputfield
     * can be overloaded by subclass
     *
     * @returns {InputField}
     * @constructor
     */
    static get InputField() {
        return InputField;
    }

    /**
     * Returns the component for CharacterButton
     * subclass can override
     * @returns {Character}
     * @constructor
     */
    static get Character() {
        return Character;
    }

    /**
     * Returns the component for ActionKey
     * subclass can override
     * @returns {Action}
     * @constructor
     */
    static get Action() {
        return Action;
    }

    /**
     * Returns the component for ActionKey
     * subclass can override
     * @returns {HotKey}
     * @constructor
     */
    static get HotKey() {
        return HotKey;
    }

    /**
     * Returns the component for email EmailKey
     * subclass can override
     * @returns {EmailKey}
     * @constructor
     */
    static get Email() {
        return Email;
    }

    /**
     * Set the keyboard title
     * @param v
     */
    set title(v) {
        this.tag("Title").text.text = `${v}`;
    }

    /**
     * Set the helper message
     * @param v
     */
    set message(v) {
        this.tag("Message").text.text = `${v}`;
    }

    /**
     * Do emailaddresses pop up when user types an @
     * @param v
     */
    set emailHelpers(v) {
        this._emailHelpers = v;
    }

    /**
     * Before displaying the actual value in the inputfield,
     * it runs through a filter (if provided)
     * @param v - filter function
     */
    set mask(v){
        this.tag("InputField").mask = v;
    }

    /**
     * Change keyboard layout
     * @param v
     */
    set layout(v){
        this._changeLayout(v || "normal")
    }

    /**
     * Set keyboard mode
     * @param v
     */
    set mode(v){
        this.tag("InputCharacters").create(Keyboard.config.layout[v], this.constructor.Character, v);
        this.tag("Actions").create(Keyboard.config.actions, this.constructor.Action);
        this.tag("HotKeys").create(Keyboard.config.hotkeys, this.constructor.HotKey);

        this._setState("Characters");
    }

    /**
     * Restore to a new value, called from parent to change
     * to a new value and restore cursor position
     * @param v
     */
    set value(v){
        this.tag("InputField").resetValue = v;
    }

    /**
     * actual emailaddresses which popup when
     * emailHelpers is true
     * @param v
     */
    set email(v) {
        if (v) {
            this.tag("EmailHelpers").create(v, this.constructor.Email);
        }
    }

    static get mask(){
        return {
            PASSWORD:(str)=>{
                return new Array(str.length).fill("●").join("");
            },
            NUMBER:(str)=>{
                return parseInt(str);
            },
            CREDITCARD:(str)=>{
                return str.replace(/(\d{4})(?=\d)/ig,'$1-');
            }
        }
    }

    /**
     * Keyboard states definition
     * @returns {Array}
     * @private
     */
    static _states() {
        return [
            class Input extends this {
                _getFocused() {
                    return this.tag("InputField");
                }

                _handleDown() {
                    this._setState("Characters");
                }
            },
            class Characters extends this {
                _getFocused() {
                    return this.tag("InputCharacters");
                }

                onKeypress({char}) {
                    const value = this.tag("InputField").feed(char);

                    // emit so other components can listen
                    this.emit("change", {char, value});

                    if (this._emailHelpers) {
                        this.toggleHelpers(this.isStartOfEmail(value));
                    }
                }

                _handleDown() {
                    const emailVisible = this.tag("EmailHelpers").visible;
                    this._setState(emailVisible ? "EmailHelpers" : "Actions");
                }

                _handleUp() {
                    this._setState("Input");
                }

                _handleRight() {
                    this._setState("HotKeys");
                }
            },
            class Actions extends this {
                $enter(){
                    const relIndex = this.tag("InputCharacters").index % 10;
                    const offset = [[0,1,2],[3,4,5,6,7,8],[9]];
                    const index = offset.reduce((a, v, i)=>{
                        return v.indexOf(relIndex) !== -1?i:a;
                    },0);

                    this.tag("Actions").setIndex(index);
                }
                _getFocused() {
                    return this.tag("Actions");
                }

                _handleUp() {
                    const emailVisible = this.tag("EmailHelpers").visible;
                    this._setState(emailVisible ? "EmailHelpers" : "Characters");
                }

                _handleEnter() {
                    const active = this.tag("Actions").activeAction;
                    if (active.action && this._hasMethod(active.action)) {
                        this[active.action]();
                    }
                }

                onClear() {
                    this.tag("InputField").change('');
                }

                onSpace() {
                    this.tag("InputField").feed(' ');
                }

                onDone() {
                    const value = this.tag("InputField").value;

                    // emit onReady so components can lister
                    this.emit("done", {value});
                }
            },
            class EmailHelpers extends this {
                _getFocused() {
                    return this.tag("EmailHelpers");
                }

                _handleEnter() {
                    const action = this.tag("EmailHelpers").activeAction.action;
                    this.tag("InputField").feed(action.substring(1, action.length));
                    this.toggleHelpers(false);
                    this._setState("Characters");
                }

                _handleUp() {
                    this._setState("Characters");
                }

                _handleDown() {
                    this._setState("Actions");
                }
            },
            class HotKeys extends this {
                $enter(){
                    const charIndex = this.tag("InputCharacters").index;
                    const row = ~~(charIndex / 10);
                    this.tag("HotKeys").setIndex(row);
                }

                _getFocused() {
                    return this.tag("HotKeys");
                }

                _handleLeft() {
                    let index = this.tag("HotKeys").index;
                    const offset = ++index*10 - 1;

                    this.tag("InputCharacters").setIndex(offset, true);
                    this._setState("Characters");
                }

                _handleDown() {
                    const emailVisible = this.tag("EmailHelpers").visible;
                    this._setState(emailVisible ? "EmailHelpers" : "Actions");
                }

                _handleEnter() {
                    const action = this.tag("HotKeys").activeAction.action;
                    if (action) {
                        if (this._hasMethod(action)) {
                            this[action]();
                        } else {
                            const reg = /layout:([a-zA-Z]+)/;
                            const matches = reg.exec(action);

                            if (matches && matches.length) {
                                this._changeLayout(matches[1]);
                            }
                        }
                    }
                }

                onDelete() {
                    const value = this.tag("InputField").delete();
                    this.toggleHelpers(this.isStartOfEmail(value));
                }
            }
        ];
    }

    isStartOfEmail(str) {
        const reg = /^[^\s@]+@$/;
        const matches = reg.exec(str);
        return !!matches;
    }

    toggleHelpers(v) {
        this.tag("EmailHelpers").visible = v;

        this.patch({
            EmailHelpers: {visible: v},
            InputCharacters: {smooth: {y: v ? 230 : 280}},
            HotKeys: {smooth: {y: v ? 230 : 280}},
            Actions: {smooth: {y: v ? 700 : 680}}
        });
    }

    _changeLayout(layout) {
        this.tag("InputCharacters").create(
            Keyboard.config.layout[layout], Keyboard.Character, layout
        );

        this.emit("layoutChange",{layout});
    }
}
