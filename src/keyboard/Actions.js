import { Lightning } from '@lightningjs/sdk';

export default class Actions extends Lightning.Component{

    static _template(){
        return {
            w:1200,
            flex:{
                direction:'row', justifyContent:'space-around'
            }
        }
    }

    create(config, constructor){
       const keys = Object.keys(config);
       this.children = keys.map((label)=>{
            return {
                type: constructor, label, action:config[label]
            }
       });

       this._index = 1;
    }

    get actions(){
        return this.children;
    }

    get activeAction(){
        return this.actions[this._index];
    }

    _handleLeft(){
        const idx = this._index;
        if(idx > 0){
            this.setIndex(idx-1);
        }else{
            return false;
        }
    }

    _handleRight(){
        const idx = this._index;
        if(idx < this.actions.length - 1){
            this.setIndex(idx+1);
        }else{
            return false;
        }
    }

    _unfocus(){
        this._index = 1;
    }

    setIndex(index){
        this._index = index;
    }

    get index(){
        return this._index;
    }

    _getFocused(){
        return this.activeAction;
    }
}