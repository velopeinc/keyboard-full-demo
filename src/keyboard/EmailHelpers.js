import Actions from "./Actions.js";

export default class EmailHelpers extends Actions {

    static _template(){
        return {
            w:1100,
            flex:{
                direction:'row', justifyContent:'space-around'
            }
        }
    }

    create(config, constructor){
        this.children = config.map((label)=>{
            return {
                type: constructor, label, action:label
            }
        });
        this._index = 0;
    }

}