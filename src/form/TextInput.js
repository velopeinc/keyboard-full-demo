export default class TextInput extends lng.Component{

    static _template(){
        return {
            Label:{
                text:{text:'no label', fontSize:60}
            },
            Input:{ y: 100,
                rect:true, w: 900, h:80, color: 0xaaffffff,
                Label:{ x:20, y:20,
                    text:{text:'Search', fontSize:30, textColor:0xaa000000}
                }
            }
        }
    }

    get tabindexed(){
        return true;
    }

    set placeholder(v){
        this._placeholder = v;
        this.tag("Input.Label").text.text = v;
    }

    set value(v){
        this._value = v;
        this.tag("Input.Label").text.text = v?v:this._placeholder;
    }

    get value(){
        return this._value || "";
    }

    set label(v){
        this.tag("Label").text.text = `${v}`;
    }
}